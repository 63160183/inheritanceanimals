/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author ACER
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani","White",0);
        animal.speak();
        animal.walk();
        System.out.println("---------");
        
        Dog dang = new Dog("Dang","Black and White");
        dang.speak();
        dang.walk();
        System.out.println("---------");
        
        Dog to = new Dog("To","Orange and White");
        dang.speak();
        dang.walk();
        System.out.println("---------");
        
        Dog mome = new Dog("Mome","White and Black");
        dang.speak();
        dang.walk();
        System.out.println("---------");
        
        Dog bat = new Dog("Bat","Black and White");
        dang.speak();
        dang.walk();
        System.out.println("---------");
        
        Cat zero = new Cat("Zero","Orange");
        zero.speak();
        zero.walk();
        System.out.println("---------");
        
        Duck som = new Duck("Som","Orange");
        som.speak();
        som.walk();
        som.fly();
        System.out.println("---------");
        
        Duck gabgab = new Duck("GabGab","Orange and Black");
        som.speak();
        som.walk();
        som.fly();
        System.out.println("---------");
        
        System.out.println("Som is Animal: " + (som instanceof Animal));
        System.out.println("Som is Duck: " + (som instanceof Duck));
        System.out.println("Som is Cat: " + (som instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));
        System.out.println("GabGab is Animal: " + (gabgab instanceof Animal));
        System.out.println("GabGab is Duck: " + (gabgab instanceof Duck));
        System.out.println("GabGab is Cat: " + (gabgab instanceof Object));
        System.out.println("---------");
        System.out.println("Zero is Animal: " + (zero instanceof Animal));
        System.out.println("Zero is Cat: " + (zero instanceof Cat));
        System.out.println("Zero is Dog: " + (zero instanceof Object));
        System.out.println("---------");
        System.out.println("Dang is Animal: " + (dang instanceof Animal));
        System.out.println("Dang is Dog: " + (dang instanceof Dog));
        System.out.println("Dang is Duck: " + (dang instanceof Object));
        System.out.println("To is Animal: " + (to instanceof Animal));
        System.out.println("To is Dog: " + (to instanceof Dog));
        System.out.println("To is Cat: " + (to instanceof Object));
        System.out.println("Mome is Animal: " + (mome instanceof Animal));
        System.out.println("Mome is Dog: " + (mome instanceof Dog));
        System.out.println("Mome is Cat: " + (mome instanceof Object));
        System.out.println("Bat is Animal: " + (bat instanceof Animal));
        System.out.println("Bat is Dog: " + (bat instanceof Dog));
        System.out.println("Bat is Duck: " + (bat instanceof Object));
        System.out.println("---------");
        
        Animal[] animals = {dang,to,mome,bat,zero,som};
        for(int i=0;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            if(animals[i] instanceof Dog){
                Dog dog = (Dog)animals[i];
                dog.walk();
                dog.speak();
            }
            if(animals[i] instanceof Cat){
                Cat cat = (Cat)animals[i];
                cat.walk();
                cat.speak();
            }
        }
    }
}
